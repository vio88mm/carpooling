<?php


class User extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;
     
    /**
     *
     * @var string
     */
    protected $last_login;
     
    /**
     *
     * @var string
     */
    protected $account_creation;
     
    /**
     *
     * @var integer
     */
    protected $status;
     
    /**
     *
     * @var integer
     */
    protected $rating;
     
    /**
     *
     * @var integer
     */
    protected $nr_of_ratings;
     
    /**
     *
     * @var string
     */
    protected $profile_image;
     
    /**
     *
     * @var string
     */
    protected $password;
     
    /**
     *
     * @var string
     */
    protected $first_name;
     
    /**
     *
     * @var string
     */
    protected $last_name;
     
    /**
     *
     * @var string
     */
    protected $description;
     
    /**
     *
     * @var integer
     */
    protected $age;
     
    /**
     *
     * @var string
     */
    protected $email;
     
    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Method to set the value of field last_login
     *
     * @param string $last_login
     * @return $this
     */
    public function setLastLogin($last_login)
    {
        $this->last_login = $last_login;
        return $this;
    }

    /**
     * Method to set the value of field account_creation
     *
     * @param string $account_creation
     * @return $this
     */
    public function setAccountCreation($account_creation)
    {
        $this->account_creation = $account_creation;
        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Method to set the value of field rating
     *
     * @param integer $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * Method to set the value of field nr_of_ratings
     *
     * @param integer $nr_of_ratings
     * @return $this
     */
    public function setNrOfRatings($nr_of_ratings)
    {
        $this->nr_of_ratings = $nr_of_ratings;
        return $this;
    }

    /**
     * Method to set the value of field profile_image
     *
     * @param string $profile_image
     * @return $this
     */
    public function setProfileImage($profile_image)
    {
        $this->profile_image = $profile_image;
        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Method to set the value of field first_name
     *
     * @param string $first_name
     * @return $this
     */
    public function setFirstName($first_name)
    {
        $this->first_name = $first_name;
        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name)
    {
        $this->last_name = $last_name;
        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Method to set the value of field age
     *
     * @param integer $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field last_login
     *
     * @return string
     */
    public function getLastLogin()
    {
        return $this->last_login;
    }

    /**
     * Returns the value of field account_creation
     *
     * @return string
     */
    public function getAccountCreation()
    {
        return $this->account_creation;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Returns the value of field nr_of_ratings
     *
     * @return integer
     */
    public function getNrOfRatings()
    {
        return $this->nr_of_ratings;
    }

    /**
     * Returns the value of field profile_image
     *
     * @return string
     */
    public function getProfileImage()
    {
        return $this->profile_image;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the value of field first_name
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the value of field age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    "field"    => "email",
                    "required" => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * @return User[]
     */
    public static function find($parameters = array())
    {
        return parent::find($parameters);
    }

    /**
     * @return User
     */
    public static function findFirst($parameters = array())
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id' => 'id', 
            'last_login' => 'last_login', 
            'account_creation' => 'account_creation', 
            'status' => 'status', 
            'rating' => 'rating', 
            'nr_of_ratings' => 'nr_of_ratings', 
            'profile_image' => 'profile_image', 
            'password' => 'password', 
            'first_name' => 'first_name', 
            'last_name' => 'last_name', 
            'description' => 'description', 
            'age' => 'age', 
            'email' => 'email'
        );
    }

}
